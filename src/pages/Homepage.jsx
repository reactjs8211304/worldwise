import { Link } from "react-router-dom";
import PageNav from "../components/PageNav";
import styles from "./Homepage.module.css";

export default function Homepage() {
  return (
    <main className={styles.homepage}>
      <PageNav />

      <section>
        <h1>
          Đi du lịch khắp thế giới.
          <br />
          WorldWise theo dõi cuộc phiêu lưu của bạn.
        </h1>
        <h2>
          Bản đồ thế giới theo dõi bước chân của bạn đến mọi thành phố bạn có
          thể nghĩ tới của. Đừng bao giờ quên những trải nghiệm tuyệt vời của
          bạn và hãy chỉ cho bạn bè bạn cách bạn đã lang thang khắp thế giới.
        </h2>
        <Link to="/login" className="cta">
          Bắt đầu du lịch bây giờ
        </Link>
      </section>
    </main>
  );
}
