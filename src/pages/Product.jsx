import PageNav from "../components/PageNav";
import styles from "./Product.module.css";

export default function Product() {
  return (
    <main className={styles.product}>
      <PageNav />

      <section>
        <img
          src="img-1.jpg"
          alt="person with dog overlooking mountain with sunset"
        />
        <div>
          <h2>Về WorldWide.</h2>
          <p>
            Bạn là một người yêu thích du lịch và muốn ghi lại những trải nghiệm
            tuyệt vời của mình trên khắp thế giới? WorldWide là địa điểm lý
            tưởng để bạn lưu giữ những kỷ niệm đáng nhớ từ mỗi chuyến phiêu lưu.
          </p>
          <p>
            Hãy đăng ký ngay hôm nay và bắt đầu ghi chú chuyến đi của bạn một
            cách đơn giản và thú vị. Đánh dấu những điểm đến, chia sẻ những câu
            chuyện của bạn, và kết nối với cộng đồng du lịch tại WorldWide!
          </p>
        </div>
      </section>
    </main>
  );
}
