// Uses the same styles as Product
import PageNav from "../components/PageNav";
import styles from "./Product.module.css";

export default function Product() {
  return (
    <main className={styles.product}>
      <PageNav />

      <section>
        <div>
          <h2>
            Mức Phí.
            <br />
            50,000 Đồng/Tháng.
          </h2>
          <p>
            Với WorldWide, chúng tôi cam kết mang đến cho bạn trải nghiệm tuyệt
            vời với mức phí hợp lý và tiết kiệm. Chỉ với 50,000 đồng mỗi tháng,
            bạn có thể tận hưởng toàn bộ các tính năng cao cấp của trang web và
            bắt đầu hành trình ghi chú chuyến đi của mình.
          </p>
        </div>
        <img src="img-2.jpg" alt="overview of a large city with skyscrapers" />
      </section>
    </main>
  );
}
