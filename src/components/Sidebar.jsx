import { Outlet } from "react-router-dom";
import AppNav from "./AppNav";
import Logo from "./Logo";
import styles from "./Sidebar.module.css";

function Sidebar() {
  return (
    <div className={styles.sidebar}>
      <Logo />
      <AppNav />

      <Outlet />

      <footer className={styles.footer}>
        {/* <p className={styles.copyright}>
          {new Date().getFullYear()} WorldWise Inc by Nguyễn Hoàng Nam.
        </p> */}
      </footer>
    </div>
  );
}

export default Sidebar;
